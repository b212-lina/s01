package com.zuitt.batch212;

import java.util.Date;
import java.util.Scanner;

public class UserInformation {

    public static void main(String[] args) {

        Scanner appScanner = new Scanner(System.in);

        System.out.println("First Name:");
        String firstName = appScanner.nextLine().trim();

        System.out.println("Last Name:");
        String lastName = appScanner.nextLine().trim();

        System.out.println("First Subject Grade:");
        Double firstSubject = appScanner.nextDouble();

        System.out.println("Second Subject Grade:");
        Double secondSubject = appScanner.nextDouble();

        System.out.println("Third Subject Grade:");
        Double thirdSubject = appScanner.nextDouble();

        Double averageGrade = (firstSubject + secondSubject + thirdSubject)/3;

        System.out.println("Good day, " + firstName + " " + lastName + ".");
        System.out.println("Your grade average is: " + averageGrade);

    }
}
